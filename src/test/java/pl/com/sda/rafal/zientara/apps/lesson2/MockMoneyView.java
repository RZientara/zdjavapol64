package pl.com.sda.rafal.zientara.apps.lesson2;

import pl.com.sda.rafal.zientara.apps.lesson2.model.Item;

import java.util.List;

public class MockMoneyView implements MoneyContract.View {
    @Override
    public void refreshList(List<Item> data) {

    }

    @Override
    public void showSum(double sum) {

    }
}
