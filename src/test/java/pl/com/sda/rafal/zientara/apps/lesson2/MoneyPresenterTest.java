package pl.com.sda.rafal.zientara.apps.lesson2;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.com.sda.rafal.zientara.apps.lesson2.model.Item;
import pl.com.sda.rafal.zientara.apps.lesson2.model.ItemProvider;
import pl.com.sda.rafal.zientara.apps.lesson2.presenter.MoneyPresenter;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MoneyPresenterTest {

    private MoneyContract.View view = Mockito.mock(MoneyContract.View.class);
    private ItemProvider provider = Mockito.mock(ItemProvider.class);
    private MoneyPresenter moneyPresenter = new MoneyPresenter(view, provider);

    @Test
    public void sampleUseMockedData(){
        // given
        List<Item> returnedValue = Arrays.asList(
                new Item("zabka", 1, LocalDate.now())
        );
        when(provider.getItems()).thenReturn(returnedValue);

        // when
        moneyPresenter.prepareData();

        // then`
        verify(view).refreshList(returnedValue);
        verify(view).showSum(1);
    }

}