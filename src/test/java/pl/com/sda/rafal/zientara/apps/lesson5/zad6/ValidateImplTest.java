package pl.com.sda.rafal.zientara.apps.lesson5.zad6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateImplTest {
    private Validate validate = new ValidateImpl();

    @Test
    public void sumBiggerThan300() {
        Parcel parcel = new Parcel(300, 30, 45, 2, false);

        assertThrows(TooBigParcelException.class,
                () -> validate.validate(parcel));
    }

    @Test
    public void xIsTooShort() {
        Parcel parcel = new Parcel(3, 30, 30, 2, false);

        assertThrows(ValueTooShortException.class,
                () -> validate.validate(parcel));
    }

    @Test
    public void yIsTooShort() {
        Parcel parcel = new Parcel(30, 3, 30, 2, false);

        assertThrows(ValueTooShortException.class,
                () -> validate.validate(parcel));
    }

    @Test
    public void zIsTooShort() {
        Parcel parcel = new Parcel(30, 30, 3, 2, false);

        assertThrows(ValueTooShortException.class,
                () -> validate.validate(parcel));
    }

    @Test
    public void weightIsTooBig() {
        Parcel parcel = new Parcel(30, 30, 30, 31, false);

        assertThrows(ParcelTooHeavyException.class,
                () -> validate.validate(parcel));
    }

    @Test
    public void weightIsTooBigForExpress() {
        Parcel parcel = new Parcel(30, 30, 30, 16, true);

        assertThrows(TooHeavyForExpressException.class,
                () -> validate.validate(parcel));
    }

    /*@Test(expected = PathTooLongException.class)
    public void sumBiggerThan300_v2() {
        Parcel parcel = new Parcel(300, 10, 10, 2, false);

        assertThrows(PathTooLongException.class,
                () -> validate.validate(parcel));
    }*/

}