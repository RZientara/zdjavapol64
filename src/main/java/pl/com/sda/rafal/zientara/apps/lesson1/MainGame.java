package pl.com.sda.rafal.zientara.apps.lesson1;

import pl.com.sda.rafal.zientara.apps.lesson1.language.EnglishTranslation;
import pl.com.sda.rafal.zientara.apps.lesson1.language.FrenchTranslation;
import pl.com.sda.rafal.zientara.apps.lesson1.language.Translation;
import pl.com.sda.rafal.zientara.apps.lesson1.player.Player;
import pl.com.sda.rafal.zientara.apps.lesson1.player.RandomPlayer;
import pl.com.sda.rafal.zientara.apps.lesson1.player.ScannerPlayer;

public class MainGame {

    public static void main(String[] args) {
        Player player1 = new RandomPlayer("Zdzichu");
//        Player player2 = () -> GameAction.SCISSORS;
        Player player2 = new ScannerPlayer("Rafał");

//        Translation translation = new FrenchTranslation();
        Translation translation = new EnglishTranslation();

        GameTicTacToe game = new GameTicTacToe(player1,
                player2, translation);

        game.startGame();
    }




}
