package pl.com.sda.rafal.zientara.apps.lesson5.zad6;

public interface Validate {
    boolean validate(Parcel parcel);
}
