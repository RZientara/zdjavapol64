package pl.com.sda.rafal.zientara.apps.lesson1.player;

import pl.com.sda.rafal.zientara.apps.lesson1.GameAction;

import java.util.Random;

public class RandomPlayer extends Player {
    private Random random = new Random();

    public RandomPlayer(String nick) {
        super(nick);
    }

    @Override
    public GameAction chooseAction() {
        int value = random.nextInt(3);
//        int randomValue = random.nextInt() % 5;
//        return getGameActionIf(value);
        return getGameActionSwitch(value);
    }

    private GameAction getGameActionSwitch(int value) {
        switch (value) {
            case 0:
                return GameAction.SCISSORS;
            case 1:
                return GameAction.ROCK;
            case 2:
                return GameAction.PAPER;
            default:
                throw new IllegalStateException();
        }
    }

    private GameAction getGameActionIf(int value) {
        if (value == 0) {
            return GameAction.SCISSORS;
        } else if(value == 1) {
            return GameAction.ROCK;
        } else {
            return GameAction.PAPER;
        }
    }
}
