package pl.com.sda.rafal.zientara.apps.lesson1.swing;

public class WincyjContract {

    public interface View {
        void refreshLabel(int clicksCount);
    }

    public interface Presenter {
        void onButtonClicked();
    }
}
