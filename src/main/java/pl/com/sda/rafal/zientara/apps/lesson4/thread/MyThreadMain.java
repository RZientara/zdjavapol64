package pl.com.sda.rafal.zientara.apps.lesson4.thread;

public class MyThreadMain {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            Thread t = new MyThread();
            t.start();
//            t.run();
//            System.out.println(Thread.currentThread().getName());
        }

    }

    static class MyThread extends Thread {
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName());
        }
    }
}
