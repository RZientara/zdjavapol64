package pl.com.sda.rafal.zientara.apps.lesson5.threads;

import java.util.Random;

public class DownloadProcess implements Runnable {
    private static int counter = 1;

    private final Progress listener;
    private Random random = new Random();
    private final int id;

    public DownloadProcess(Progress listener) {
        this.listener = listener;
        id = counter++;
    }

    public int getId() {
        return id;
    }

    public interface Progress {
        void onProgress(int key, int percent);

        void onComplete();
//        void onError();
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        System.out.println();
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(random.nextInt(200));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int progress = (i + 1) * 10;
            listener.onProgress(id, progress);
            System.out.println(name + " progress: " + progress + "%");
        }
        System.out.println(name + " Complete!");
        listener.onComplete();
    }
}
