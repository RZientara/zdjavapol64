package pl.com.sda.rafal.zientara.apps.lesson1.language;

import pl.com.sda.rafal.zientara.apps.lesson1.GameAction;
import pl.com.sda.rafal.zientara.apps.lesson1.player.Player;

public class EnglishTranslation implements Translation {
    @Override
    public String actionText1(GameAction action1, Player player1) {
        return "Action 1: " + action1;
    }

    @Override
    public String actionText2(GameAction action2, Player player2) {
        return String.format("%s selected action: %s", player2.nick, getAction(action2));
    }

    @Override
    public String player1Win(Player player1) {
        return player1.nick + " wins!";
    }

    String getAction(GameAction action) {
        switch (action) {
            default:
            case SCISSORS:
                return "Scissors";
            case PAPER:
                return "Paper";
            case ROCK:
                return "Rock";
        }
    }
}
