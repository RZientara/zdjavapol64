package pl.com.sda.rafal.zientara.apps.lesson1.player;

import pl.com.sda.rafal.zientara.apps.lesson1.GameAction;

import java.util.Scanner;

public class ScannerPlayer extends Player {

    public ScannerPlayer(String nick) {
        super(nick);
    }

    @Override
    public GameAction chooseAction() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose action: r/p/s");
        String input;
        do {
            input = scanner.next();

            switch (input.toLowerCase()) {
                case "r":
                case "rock":
                    return GameAction.ROCK;
                case "p":
                case "paper":
                    return GameAction.PAPER;
                case "s":
                case "scissors":
                    return GameAction.SCISSORS;
                default:
                    System.out.println("Wrong input!");
            }
        } while (true);

    }
}
