package pl.com.sda.rafal.zientara.apps.lesson5.zad6;

public class ValidateImpl implements Validate {
    private static final int MIN_LENGTH = 30;
    private static final int SUM_LIMIT = 300;
    private static final float WEIGHT_LIMIT_KG = 30f;
    private static final float WEIGHT_LIMIT_KG_FOR_EXPRESS = 15f;

    @Override
    public boolean validate(Parcel parcel) {
        if (parcel.x + parcel.y + parcel.z > SUM_LIMIT)
            throw new TooBigParcelException();

        if (parcel.x < MIN_LENGTH || parcel.y < MIN_LENGTH || parcel.z < MIN_LENGTH) {
            throw new ValueTooShortException();
        }

        if (!parcel.isExpress && parcel.weight > WEIGHT_LIMIT_KG) {
            throw new ParcelTooHeavyException();
        }

        if (parcel.isExpress && parcel.weight > WEIGHT_LIMIT_KG_FOR_EXPRESS) {
            throw new TooHeavyForExpressException();
        }

        return false;
    }
}
