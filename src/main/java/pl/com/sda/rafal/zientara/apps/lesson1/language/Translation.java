package pl.com.sda.rafal.zientara.apps.lesson1.language;

import pl.com.sda.rafal.zientara.apps.lesson1.GameAction;
import pl.com.sda.rafal.zientara.apps.lesson1.player.Player;

public interface Translation {
    String actionText1(GameAction action1, Player player1);

    String actionText2(GameAction action2, Player player2);

    String player1Win(Player player1);
}
