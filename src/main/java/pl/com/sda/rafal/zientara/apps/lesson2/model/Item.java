package pl.com.sda.rafal.zientara.apps.lesson2.model;

import pl.com.sda.rafal.zientara.apps.lesson2.MoneyContract;

import java.time.LocalDate;

public class Item {
    public final String shop;
    public final double price;
    public final LocalDate date;

    public Item(String shop, double price, LocalDate date) {
        this.shop = shop;
        this.price = price;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Item{" +
                "shop='" + shop + '\'' +
                ", price=" + price +
                ", date=" + date +
                '}';
    }
}
