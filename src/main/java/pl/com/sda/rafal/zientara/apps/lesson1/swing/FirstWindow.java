package pl.com.sda.rafal.zientara.apps.lesson1.swing;

import javax.swing.*;

public class FirstWindow {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Hello");
        JLabel label = new JLabel("World!");
        frame.add(label);
        frame.setSize(300,300);
        frame.setVisible(true);
    }
}
