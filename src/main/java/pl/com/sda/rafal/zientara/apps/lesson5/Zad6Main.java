package pl.com.sda.rafal.zientara.apps.lesson5;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;

public class Zad6Main {

    public static void main(String[] args) {
        TreeSet<Integer> treeSet = new TreeSet<>();
        Optional<Integer> first = getFirst(treeSet);
        Integer last = getLast(treeSet);
    }

    private static Integer getLast(TreeSet<Integer> treeSet) {
        Iterator<Integer> iterator = treeSet.iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (!iterator.hasNext()) {
                return next;
            }
        }
        return null;
    }

    private static Optional<Integer> getFirst(TreeSet<Integer> treeSet) {
        Iterator<Integer> iterator = treeSet.iterator();
        if(iterator.hasNext()) {
            Integer next = iterator.next();
            return Optional.of(next);
        }
        return Optional.empty();
    }
}
