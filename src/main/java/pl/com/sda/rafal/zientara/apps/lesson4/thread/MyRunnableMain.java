package pl.com.sda.rafal.zientara.apps.lesson4.thread;

public class MyRunnableMain {

    public static void main(String[] args) {
        Runnable r = new MyRunnable();
        Thread t = new Thread(r);
        t.start();
    }

    static class MyRunnable implements Runnable {

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName());
        }

    }
}
