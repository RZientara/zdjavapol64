package pl.com.sda.rafal.zientara.apps.lesson2.view;

import pl.com.sda.rafal.zientara.apps.lesson2.MoneyContract;
import pl.com.sda.rafal.zientara.apps.lesson2.view.listener.KeyReleasedListener;
import pl.com.sda.rafal.zientara.apps.lesson2.model.Item;
import pl.com.sda.rafal.zientara.apps.lesson2.model.ShopingProvider;
import pl.com.sda.rafal.zientara.apps.lesson2.presenter.MoneyPresenter;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.List;

public class WhereIsMyMoney implements MoneyContract.View {
    private static final int FIELD_WIDTH = 400;
    private static final int FIELD_HEIGHT = 50;
    private static final int PADDING = 50;
    private final JLabel sumLabel;
    private JFrame frame;
    private JTextField shopInput;
    private JTextField dateFrom;
    private JTextField dateTo;
    private JTextField costFrom;
    private JTextField costTo;
    private JList<Item> results;
    private MoneyContract.Presenter presenter =
            new MoneyPresenter(this, new ShopingProvider());

    public WhereIsMyMoney() {
        frame = new JFrame("Koszta");
        frame.setSize(FIELD_WIDTH + 2 * PADDING, 700);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JLabel names = new JLabel("Filter by name");
        names.setBounds(PADDING, 0, FIELD_WIDTH, FIELD_HEIGHT);
        frame.add(names);
        shopInput = new JTextField();
        shopInput.setBounds(PADDING, PADDING, FIELD_WIDTH, FIELD_HEIGHT);
        shopInput.addKeyListener(new KeyReleasedListener() {
            @Override
            public void keyReleased(KeyEvent e) {
                System.out.println("keyReleased:" + shopInput.getText());
                presenter.onNameChange(shopInput.getText());
            }
        });
        frame.add(shopInput);

        JLabel fromToDate = new JLabel("Filter by date from to");
        fromToDate.setBounds(PADDING, 100, FIELD_WIDTH, FIELD_HEIGHT);
        frame.add(fromToDate);
        dateFrom = new JTextField();
        dateFrom.setBounds(PADDING, 150, 75, FIELD_HEIGHT);
        frame.add(dateFrom);

        dateTo = new JTextField();
        dateTo.setBounds(175, 150, 75, FIELD_HEIGHT);
        frame.add(dateTo);

        JLabel fromToCost = new JLabel("Filter by price from to");
        fromToCost.setBounds(PADDING, 200, FIELD_WIDTH, FIELD_HEIGHT);
        frame.add(fromToCost);
        costFrom = new JTextField();
        costFrom.setBounds(PADDING, 250, 75, FIELD_HEIGHT);
        frame.add(costFrom);

        costTo = new JTextField();
        costTo.setBounds(175, 250, 75, FIELD_HEIGHT);
        frame.add(costTo);

        results = new JList<>();
        results.setBounds(PADDING, 350, FIELD_WIDTH, 200);
        frame.add(results);

        sumLabel = new JLabel();
        sumLabel.setBounds(PADDING, 550, FIELD_WIDTH, FIELD_HEIGHT);
        frame.add(sumLabel);

        frame.setVisible(true);
        presenter.prepareData();
    }

    @Override
    public void refreshList(List<Item> data) {
        DefaultListModel<Item> list = new DefaultListModel<>();
        for (Item cost : data) {
            list.addElement(cost);
        }
        results.setModel(list);
    }

    @Override
    public void showSum(double sum) {
        sumLabel.setText(String.valueOf(sum));
    }
}
