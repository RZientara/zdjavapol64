package pl.com.sda.rafal.zientara.apps.lesson2.model;

import java.util.List;

public interface ItemProvider {
    List<Item> getItems();
}
