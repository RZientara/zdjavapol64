package pl.com.sda.rafal.zientara.apps.lesson5;

import java.util.*;

public class Zad4Main {

    public static void main(String[] args) {
        Storage storage = new StorageImpl();
        storage.addToStorage("Java", "cool");
        storage.addToStorage("Java", "extra");
        storage.addToStorage("Java", "bajera");

        storage.addToStorage("Kotlin", "keczup");
        storage.addToStorage("Kotlin", "jezyk programowania");

        storage.addToStorage("C#", "extra");

//        storage.printValues("Kotlin");
        storage.findValues("extra");
    }

    interface Storage {
        /**
         * Dodaje elementy do Storage, pod jednym kluczem moze byc wiele wartości
         * @param key - klucz glowny
         * @param value - nowa wartosc dodana pod kluczem key
         */
        void addToStorage(String key, String value);

        void printValues(String key);

        void findValues(String value);
    }

    static class StorageImpl implements Storage {
        private Map<String, List<String>> mapper = new HashMap<>();

        @Override
        public void addToStorage(String key, String value) {
            if (mapper.containsKey(key)) {
                //lista juz istnieje, dodaje elementy
                List<String> values = mapper.get(key);
                values.add(value);
//                mapper.put(key, values);
// referencja juz zawsze bedzie taka sama
                //nie trzeba przypisywac wartosci
            } else {
                //dodajemy po raz pierwszy
                List<String> values = new ArrayList<>();
                values.add(value);
                mapper.put(key, values);
            }
        }

        @Override
        public void printValues(String key) {
            List<String> values = mapper.get(key);
            for (String val : values) {
                System.out.println(val);
            }
        }

        @Override
        public void findValues(String value) {
            for (Map.Entry<String, List<String>> entry : mapper.entrySet()) {
                List<String> values = entry.getValue();

                Optional<String> optional = values.stream()
                        .filter(text -> text.equals(value))
                        .findFirst();

                if (optional.isPresent()) {
                    System.out.println(entry.getKey());
                }
            }
        }
    }
}
