package pl.com.sda.rafal.zientara.apps.lesson5.threads;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class Janusz implements DownloadProcess.Progress {

    private AtomicInteger processCount = new AtomicInteger();
    private AtomicInteger completed = new AtomicInteger();
    private final Map<Integer, Integer> progress = new HashMap<>();

    @Override
    public void onProgress(int id, int percent) {
        synchronized (progress) {
            progress.put(id, percent);

            Optional<Integer> reduce = progress.values().stream()
                    .reduce(Integer::sum);
            int maxProgress = processCount.get() * 100;
            System.out.printf("Total progress %d/%d \n", reduce.orElse(0), maxProgress);
        }
    }

    @Override
    public void onComplete() {
        completed.addAndGet(1);
        if (completed.get() == processCount.get()) {
            System.out.println("Wszystko zrobione!");
        }
    }

    public void requestDownload(int count) {
        processCount.addAndGet(count);
        for (int i = 0; i < count; i++) {
            DownloadProcess process = new DownloadProcess(this);
            synchronized (progress) {
                this.progress.put(process.getId(), 0);
            }
            Thread thread = new Thread(process);
            thread.start();
        }
    }

}
