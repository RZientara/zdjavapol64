package pl.com.sda.rafal.zientara.apps.lesson5;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Zad3Main {

    public static void main(String[] args) {
        Map<String, Integer> collection = new HashMap<>();
        collection.put("Java", 11);
        collection.put("Kotlin", 13);
        //sposob 1
//        for (Map.Entry<String, Integer> entry : collection.entrySet()) {
//            System.out.printf("Klucz: %s, Wartosc: %d,\n", entry.getKey(), entry.getValue());
//        }
        Iterator<Map.Entry<String, Integer>> iterator = collection.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            System.out.printf("Klucz: %s, Wartosc: %d", entry.getKey(), entry.getValue());
            if (iterator.hasNext()) {
                System.out.print(",");
            } else {
                System.out.print(".");
            }
            System.out.println();
        }

    }
}
