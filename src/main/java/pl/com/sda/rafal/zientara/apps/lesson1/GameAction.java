package pl.com.sda.rafal.zientara.apps.lesson1;

public enum GameAction {
    ROCK,
    PAPER,
    SCISSORS
}
