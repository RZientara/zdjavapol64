package pl.com.sda.rafal.zientara.apps.lesson2.view.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public abstract class KeyReleasedListener implements KeyListener {
    @Override
    public void keyTyped(KeyEvent e) {
        // no-op
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //no-op
    }
}
