package pl.com.sda.rafal.zientara.apps.lesson5;

import java.util.*;
import java.util.stream.Collectors;

public class Zad12Main {

    public static final int RANDOM_ARRAY_SIZE = 100_000;

    public static void main(String[] args) {
        int[] randomNumbers = getRandomArray();

        //zad 1 - unikalne elementy
//        zad1pierwszeRozwiazanie(randomNumbers);
//        zad2drugieRozwiazanie(randomNumbers);

        //zad 3 najczesciej powtarzajace sie
        zad3(randomNumbers);
    }

    private static void zad3(int[] randomNumbers) {
        Map<Integer, Integer> occurences = new HashMap<>();
        for (int number : randomNumbers) {
            if (occurences.containsKey(number)) {
                int count = occurences.get(number);
                occurences.put(number, ++count);//preinkrementacja
            } else {
                occurences.put(number, 1);
            }
        }

        List<Map.Entry<Integer, Integer>> collect = occurences.entrySet().stream()
//                .filter(entry -> entry.getValue() > 1)//zadanie 2 rozwiazane!
                .sorted((o1, o2) -> Integer.compare(o2.getValue(), o1.getValue()))
                .collect(Collectors.toList());

        for (int i = 0; i < collect.size() && i < 25; i++) {
            System.out.println(collect.get(i));
        }
    }

    private static void zad2drugieRozwiazanie(int[] randomNumbers) {
        Set<Integer> set = new HashSet<>();
        for (int value : randomNumbers) {
            set.add(value);
        }
        for (int value : set) {
            System.out.println(value);
        }
    }

    private static void zad1pierwszeRozwiazanie(int[] randomNumbers) {
        int[] output = Arrays.stream(randomNumbers).distinct().toArray();
        for (int value : output) {
            System.out.println(value);
        }
    }

    private static int[] getRandomArray() {
        int[] randomNumbers = new int[RANDOM_ARRAY_SIZE];
        Random random = new Random();
        for (int i = 0; i < randomNumbers.length; i++) {
            randomNumbers[i] = random.nextInt(50);
        }
        return randomNumbers;
    }
}
