package pl.com.sda.rafal.zientara.apps.lesson1;

import pl.com.sda.rafal.zientara.apps.lesson1.language.Translation;
import pl.com.sda.rafal.zientara.apps.lesson1.player.Player;

public class GameTicTacToe {
    private final Player player1;
    private final Player player2;
    private final Translation translation;

    public GameTicTacToe(Player player1,
                         Player player2,
                         Translation translation) {
        this.player1 = player1;
        this.player2 = player2;
        this.translation = translation;
    }

    public void startGame() {
        GameAction action1 = player1.chooseAction();
        GameAction action2 = player2.chooseAction();
        System.out.println(translation.actionText1(action1, player1));
        System.out.println(translation.actionText2(action2, player2));
//        System.out.println("Action 1 : " + action1);
//        System.out.println("Action 2 : " + action2);

        GameResult result = checkResult(action1, action2);
        switch (result) {
            case PLAYER_1_WIN:
                System.out.println(translation.player1Win(player1));
        }
        System.out.println(result);
    }

    private GameResult checkResult(GameAction action1,
                                         GameAction action2) {
        if (action1 == null || action2 == null) {
            throw new IllegalStateException("Null actions!");
        }
        //remis
        if (action1 == action2) {
            return GameResult.DRAW;
        }
        if ((action1 == GameAction.ROCK && action2 == GameAction.SCISSORS) ||
                (action1 == GameAction.PAPER && action2 == GameAction.ROCK) ||
                (action1 == GameAction.SCISSORS && action2 == GameAction.PAPER)) {
            return GameResult.PLAYER_1_WIN;
        }
        return GameResult.PLAYER_2_WIN;
    }

}
