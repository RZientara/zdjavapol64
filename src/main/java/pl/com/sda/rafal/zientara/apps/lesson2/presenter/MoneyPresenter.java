package pl.com.sda.rafal.zientara.apps.lesson2.presenter;

import pl.com.sda.rafal.zientara.apps.lesson2.MoneyContract;
import pl.com.sda.rafal.zientara.apps.lesson2.model.Item;
import pl.com.sda.rafal.zientara.apps.lesson2.model.ItemProvider;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MoneyPresenter implements MoneyContract.Presenter {
    private final MoneyContract.View view;
    private final ItemProvider provider;

    public MoneyPresenter(MoneyContract.View view, ItemProvider provider) {
        this.view = view;
        this.provider = provider;
    }

    @Override
    public void prepareData() {
        List<Item> items = provider.getItems();
        refreshContent(items);
    }

    private void refreshContent(List<Item> items) {
        view.refreshList(items);

        double sum = 0;
        for (Item currentItem : items) {
            sum += currentItem.price;
        }

        view.showSum(sum);
    }

    @Override
    public void onNameChange(String name) {
        List<Item> output = provider.getItems()
                .stream()//stream<Item>
                .filter(item -> item.shop.contains(name))//Stream<Item>
                .collect(Collectors.toList());
        refreshContent(output);
    }

    @Override
    public void onPriceFromChange(double priceFrom) {
        //TODO
    }

    private Optional<Double> getSumOptional(List<Item> items) {
        return items.stream()//Stream<Item>
                .map(item -> item.price)//Stream<Double>
                .reduce((identity, accumulator) -> identity + accumulator);
    }

    private double getSum1(List<Item> items) {
        List<Double> prices = items.stream()//Stream<Item>
                .map(item -> item.price)//Stream<Double>
                .collect(Collectors.toList());//List<Double>

        double sum = 0;
        for (Double price : prices) {
            sum += price;
        }
        return sum;
    }
}
