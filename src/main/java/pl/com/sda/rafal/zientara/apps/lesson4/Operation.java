package pl.com.sda.rafal.zientara.apps.lesson4;

public enum Operation {
    PLUS,
    MINUS,
    MULTIPLY,
    DIVIDE
}
