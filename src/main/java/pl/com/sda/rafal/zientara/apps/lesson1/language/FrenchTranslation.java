package pl.com.sda.rafal.zientara.apps.lesson1.language;

import pl.com.sda.rafal.zientara.apps.lesson1.GameAction;
import pl.com.sda.rafal.zientara.apps.lesson1.player.Player;

public class FrenchTranslation implements Translation {

    @Override
    public String actionText1(GameAction action1, Player player1) {
        return "Makafą 1: " + action1;
    }

    @Override
    public String actionText2(GameAction action2, Player player2) {
        return String.format("Makafą 2: %s", action2);
    }

    @Override
    public String player1Win(Player player1) {
        return player1 + " fiu fiu!";
    }
}
