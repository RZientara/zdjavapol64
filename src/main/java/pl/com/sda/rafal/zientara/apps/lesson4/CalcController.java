package pl.com.sda.rafal.zientara.apps.lesson4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class CalcController {

    @FXML
    Button buttonAc;
    @FXML
    Button buttonClear;
    @FXML
    Button button7;
    @FXML
    Button button8;
    @FXML
    Button button9;
    @FXML
    Button buttonPlus;
    @FXML
    Button button4;
    @FXML
    Button button5;
    @FXML
    Button button6;
    @FXML
    Button buttonMinus;
    @FXML
    Button button1;
    @FXML
    Button button2;
    @FXML
    Button button3;
    @FXML
    Button buttonMultiply;
    @FXML
    Button button0;
    @FXML
    Button buttonComa;
    @FXML
    Button buttonEquals;
    @FXML
    Button buttonDivide;
    @FXML
    TextField outputTextField;

    private Operation currentOperation = null;
    private double previousNumber = 0;

    @FXML
    public void onClickNumber(ActionEvent event) {//#onClick w pliku .fxml
        System.out.println(Thread.currentThread().getName());
        System.out.println("numerek! " + event);
        Object source = event.getSource();
        if (source instanceof Button) {
//        if (source.getClass().equals(Button.class)) {
            Button button = (Button) source;
            String text = button.getText();
            outputTextField.appendText(text);
        }
    }

    @FXML
    public void onClickAC() {//#onClick w pliku .fxml
        System.out.println("onClickAC!");
    }

    @FXML
    public void onClickC() {//#onClick w pliku .fxml
        System.out.println("onClickAC!");
    }

    @FXML
    public void onClickPlus() {//#onClick w pliku .fxml
        setOperation(Operation.PLUS);
    }

    @FXML
    public void onClickMinus() {//#onClick w pliku .fxml
        setOperation(Operation.MINUS);
    }

    @FXML
    public void onClickMultiply() {//#onClick w pliku .fxml
        setOperation(Operation.MULTIPLY);
    }

    @FXML
    public void onClickDivide() {//#onClick w pliku .fxml
        setOperation(Operation.DIVIDE);
    }

    private void setOperation(Operation operation) {
        currentOperation = operation;
        previousNumber = getNumber();
        outputTextField.clear();
        System.out.println(operation);
    }

    public double getNumber() {
        String text = outputTextField.getText();
        try {
            return Double.parseDouble(
                    text.replace(",", "."));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @FXML
    public void onClickEquals() {//#onClick w pliku .fxml
        double b = getNumber();
        double c = getResult(previousNumber, b);
        outputTextField.setText(String.format("%f", c));
    }

    private double getResult(double a, double b) {
        switch (currentOperation) {
            case DIVIDE:
                return a / b;
            case MULTIPLY:
                return a * b;
            case MINUS:
                return a - b;
            case PLUS:
                return a + b;
            default:
                System.out.println("ERROR: Unsupported operation");
                return 0;
        }
    }

    @FXML
    public void onClickComa() {//#onClick w pliku .fxml
        System.out.println("onClickC!");
    }

}
