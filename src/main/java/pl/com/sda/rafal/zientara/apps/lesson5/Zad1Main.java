package pl.com.sda.rafal.zientara.apps.lesson5;

import java.util.ArrayList;
import java.util.Collections;

public class Zad1Main {

    public static void main(String[] args) {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Ewa");
        lista.add("Jola");
        lista.add("Ola");
        lista.add("edek");
        ArrayList<String> listaPosortowana = posortujListeOdZA(lista);
        System.out.println(listaPosortowana);
    }

    public static ArrayList<String> posortujListeOdZA(ArrayList<String> lista) {
        Collections.sort(lista);
        Collections.reverse(lista);
        return lista;
    }

}
