package pl.com.sda.rafal.zientara.apps.lesson1.swing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WincyjWindow implements WincyjContract.View {
    private JFrame frame;
    private JButton button;
    private JLabel label;
    private WincyjContract.Presenter presenter =
            new WincyjPresenter(this);

    public WincyjWindow() {
        frame = new JFrame("Klikaj");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        button = new JButton("Wincyj");
        button.setBounds(50,50,100,50);
        frame.add(button);

        label = new JLabel("0");
        label.setBounds(50,150,100,50);
        frame.add(label);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.onButtonClicked();
            }
        });

        frame.setSize(200,250);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void refreshLabel(int clicksCount) {
        label.setText(Integer.toString(clicksCount));
    }
}
