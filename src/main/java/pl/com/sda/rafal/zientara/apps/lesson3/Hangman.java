package pl.com.sda.rafal.zientara.apps.lesson3;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

public class Hangman {

    public static final int MAX_HP = 7;
    private String puzzle = "";
    private Set<String> guessList = new HashSet<>();
    private int hp = MAX_HP;

    public void setPuzzle(String puzzle) {
        this.puzzle = puzzle;
        guessList.clear();
        hp = MAX_HP;
    }

    public String getOutput() {
        return Stream.of(puzzle.split(""))
                .map(letter -> {
                    if (Character.isWhitespace(letter.charAt(0))) {
                        return " ";
                    } else {
                        if (guessList.contains(letter.toLowerCase())) {
                            return letter;
                        }
                        return ".";
                    }
                })//Stream<String> - po 1 znaku
                .reduce((element, accumulator) -> element + accumulator).get();
    }

    /**
     * Drugie rozwiazanie za pomoca for
     * @return
     */
    public String getOutputWithFor() {
        String output = "";
        for (int i = 0; i < puzzle.length(); i++) {
            String character = puzzle.substring(i, i + 1);

            if (" ".equals(character)) {
                output += " ";
            } else if (guessList.contains(character.toLowerCase())) {
                output += character;
            } else {
                output += ".";
            }
        }
        return output;
    }

    public void guess(String guess) {
        if (hp > 0) {
            String normalizedGuess = guess.trim().toLowerCase();
            if (normalizedGuess.length() == 0) {
                return;
            }
            if (normalizedGuess.length() == 1) {
                guessList.add(normalizedGuess);
                if (!puzzle.toLowerCase().contains(normalizedGuess)) {
                    hp--;
                }
            } else if (normalizedGuess.equalsIgnoreCase(puzzle)) {
                for (int i = 0; i < normalizedGuess.length(); i++) {
                    String character = normalizedGuess.substring(i, i + 1);
//                guessList.add(character);//lub tak
                    guess(character);
                }
            } else {
                hp--;
            }
        }
    }

    public Set<String> getTries() {
        Set<String> output = new TreeSet<>(guessList);
        return output;
    }

    public int getHp() {
        return hp;
    }

    public boolean isWin() {
        return getOutput().equals(puzzle);
    }

    public boolean isLose() {
        return hp == 0;
    }

    public boolean isGameOver() {
        return isWin() || isLose();
    }
}
