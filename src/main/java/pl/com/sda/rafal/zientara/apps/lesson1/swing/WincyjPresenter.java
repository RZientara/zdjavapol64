package pl.com.sda.rafal.zientara.apps.lesson1.swing;

public class WincyjPresenter implements WincyjContract.Presenter {
    private final WincyjContract.View view;

    private int clicksCount = 0;

    public WincyjPresenter(WincyjContract.View view) {
        this.view = view;
    }

    @Override
    public void onButtonClicked() {
        clicksCount++;
        view.refreshLabel(clicksCount);
    }
}
