package pl.com.sda.rafal.zientara.apps.lesson5;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Zad2Main {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Szymon", "Daria", "Zbychu", "Azor");

        Collections.sort(list, (o1, o2) -> {
            char c1 = o1.toLowerCase().charAt(0);
            char c2 = o2.toLowerCase().charAt(0);
            return Character.compare(c1, c2);
        });

        for (String text : list) {
            System.out.println(text);
        }
    }


}
