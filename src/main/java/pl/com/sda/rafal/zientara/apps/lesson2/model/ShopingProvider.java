package pl.com.sda.rafal.zientara.apps.lesson2.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ShopingProvider implements ItemProvider {
    //    private static DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private List<Item> items = new ArrayList<>();

    public ShopingProvider() {
        loadFile();
    }

    @Override
    public List<Item> getItems() {
        return items;
    }

    public void loadFile() {
        System.out.println("zyje!");
        File file = new File("zakupy.csv");
        try {
            System.out.println("File exists?:" + file.exists());
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader =
                    new BufferedReader(reader);

            String line;
            line = bufferedReader.readLine();
            boolean isHeader = true;
            while (line != null) {
                if (!isHeader) {
                    System.out.println(line);
                    parseAndAdd(line);
                } else {
                    isHeader = false;
                }
                line = bufferedReader.readLine();
            }
            bufferedReader.close();//wazne

        } catch (Exception e) {
            items.clear();
            System.out.println("Failed to load file!");
            e.printStackTrace();
        }
    }

    private void parseAndAdd(String line) {
        String[] split = line.split(";");
        String shop = split[0];
        double price = Double.parseDouble(split[1]
                .replace(",", ".")
                .replace("\"", ""));
        LocalDate date = LocalDate.parse(split[2], format);
        Item item = new Item(shop, price, date);
        items.add(item);
    }

    public static void main(String[] args) {
        new ShopingProvider().loadFile();
    }


}
