package pl.com.sda.rafal.zientara.apps.lesson2;

import pl.com.sda.rafal.zientara.apps.lesson2.model.Item;

import java.util.List;

public class MoneyContract {

    public interface View {
        public void refreshList(List<Item> data);

        void showSum(double sum);
    }

    public interface Presenter{
        void prepareData();

        void onNameChange(String name);

        void onPriceFromChange(double priceFrom);
    }
}
