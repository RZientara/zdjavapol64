package pl.com.sda.rafal.zientara.apps.lesson1.player;

import java.util.Objects;

public class Data {
    final int value;

    public Data(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return value == data.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public static void main(String[] args) {
        Data  data1 = new Data(1);
        Data  data2 = new Data(1);

        if(data1.equals(data2)) {//true trylko jesli nadpiszemy equals
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }

}
