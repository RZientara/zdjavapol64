package pl.com.sda.rafal.zientara.apps.lesson5.zad6;

public class ParcelMain {

    public static void main(String[] args) {
        //                  WRACAMY o 19:50!

        int x = 40;
        int y = 40;
        int z = 40;
        float weight = 10f;
        boolean isExpress = true;

        Parcel parcel = new Parcel(x, y, z, weight, isExpress);
        Validate validate = new ValidateImpl();
        try {
            validate.validate(parcel);
        } catch (ParcelTooHeavyException e) {
            System.out.println("Paczka jest za ciezka");
        } catch (TooBigParcelException e) {
            System.out.println("Paczka jest za duża!");
        } catch (TooHeavyForExpressException e) {
            System.out.println("Paczka za cięzka na przesyłkę ekspresową!");
        } catch (ValueTooShortException e) {
            System.out.println("Wymiary paczki za małe!");
        }
    }
}
