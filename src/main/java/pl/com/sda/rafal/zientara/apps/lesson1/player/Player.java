package pl.com.sda.rafal.zientara.apps.lesson1.player;

import pl.com.sda.rafal.zientara.apps.lesson1.GameAction;

//ctrl + alt + B - pokazuje klasy ktore bazuja na klasie/interface
public abstract class Player {
    public final String nick;

    public Player(String nick) {
        this.nick = nick;
    }

    public abstract GameAction chooseAction();
}
