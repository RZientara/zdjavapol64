package pl.com.sda.rafal.zientara.apps.lesson4.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class LikesThreadMain {

    public static void main(String[] args) {
        AtomicInteger likesCount = new AtomicInteger();

        for (int i = 0; i < 100_000; i++) {
            Liker liker = new Liker(likesCount);
            Thread thread = new Thread(liker);
            thread.start();

            Liker2 liker2 = new Liker2(likesCount);
            Thread thread2 = new Thread(liker2);
            thread2.start();
        }

    }

    static class Liker implements Runnable {
        private final AtomicInteger reference;

        Liker(AtomicInteger reference) {
            this.reference = reference;
        }

        @Override
        public void run() {
            synchronized (reference) {
                int currentLikes = reference.get();
                currentLikes++;
                reference.set(currentLikes);
                System.out.println(currentLikes);
            }
        }
    }

    static class Liker2 implements Runnable {
        private final AtomicInteger reference;

        Liker2(AtomicInteger reference) {
            this.reference = reference;
        }

        @Override
        public void run() {
            synchronized (reference) {
                int currentLikes = reference.get();
                currentLikes++;
                reference.set(currentLikes);
                System.out.println(currentLikes);
            }
        }
    }

}
