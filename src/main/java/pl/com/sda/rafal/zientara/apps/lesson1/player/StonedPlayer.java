package pl.com.sda.rafal.zientara.apps.lesson1.player;

import pl.com.sda.rafal.zientara.apps.lesson1.GameAction;

public class StonedPlayer extends Player {

    public StonedPlayer(String nick) {
        super(nick);
    }

    public StonedPlayer() {
        super("Snoop Dog");
    }

    public GameAction chooseAction() {
        return GameAction.ROCK;
    }

}
